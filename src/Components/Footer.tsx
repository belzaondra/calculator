import { Box, Flex, Icon, IconButton, Text } from "@chakra-ui/react";
import React from "react";
import { FaCopyright, FaLinkedinIn } from "react-icons/fa";

interface FooterProps {}

export const Footer: React.FC<FooterProps> = () => {
  return (
    <>
      <Box pt={4}>
        <Flex align="center" justify="center">
          <Text w={4} mr={2}>
            <Icon as={FaCopyright} />
          </Text>

          <Text textAlign="center">Ondrej Belza</Text>
          <IconButton
            as="a"
            target="_blank"
            href="https://www.linkedin.com/in/ond%C5%99ej-belza-b8484a187/"
            ml={2}
            size="sm"
            aria-label="Linked in link"
            icon={<Icon as={FaLinkedinIn} />}
          ></IconButton>
        </Flex>
      </Box>
    </>
  );
};
