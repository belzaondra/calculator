import { IconButton } from "@chakra-ui/button";
import { MoonIcon, SunIcon } from "@chakra-ui/icons";
import { Box, Flex, Heading } from "@chakra-ui/layout";
import { useColorMode } from "@chakra-ui/react";
import React from "react";
interface NavbarProps {}

export const Navbar: React.FC<NavbarProps> = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Flex align="center" justify="center">
      <Flex w="80%" align="center" p={2}>
        <Heading>Calculator</Heading>

        <Box ml="auto">
          <IconButton
            aria-label="color mode switcher"
            icon={colorMode === "dark" ? <SunIcon /> : <MoonIcon />}
            onClick={toggleColorMode}
          />
        </Box>
      </Flex>
    </Flex>
  );
};
