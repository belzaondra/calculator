import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { ChakraProvider, ColorModeScript, theme } from "@chakra-ui/react";
import { Navbar } from "./Components/Navbar";
import { Footer } from "./Components/Footer";

ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <ColorModeScript initialColorMode={theme.config.initialColorMode} />
      <Navbar />
      <App />
      <Footer />
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
