import {
  Box,
  Button,
  Flex,
  Input,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useState } from "react";
import "./App.css";

type ButtonType = "operation" | "number" | "reset";
interface ButtonValue {
  value: string;
  type: ButtonType;
}

function App() {
  const [expression, setExpression] = useState("");
  const [inputValue, setInputValue] = useState("");

  const disableOperation = () =>
    lastButtonPressed === "operation" || lastButtonPressed === "reset";

  const [lastButtonPressed, setLastButtonPressed] = useState<ButtonType | null>(
    "reset"
  );
  const [clearOnNextInput, setClearOnNextInput] = useState<boolean>(false);
  const stackBackgroundColor = useColorModeValue("#A7BECC", "grey.800");
  const topRowButtonsBackgroundColor = useColorModeValue(
    "gray.500",
    "whiteAlpha.400"
  );

  const handleOperation = (value: string) => {
    switch (value) {
      case "±":
        // eslint-disable-next-line no-eval
        return setInputValue(eval(`${inputValue}*-1`));
      case "%":
      case "-":
      case "*":
      case "/":
      case "+": {
        setExpression(`${expression} ${inputValue} ${value}`);
        setInputValue("");
        break;
      }
      case "=": {
        // eslint-disable-next-line no-eval
        setInputValue(eval(expression + inputValue));
        setExpression("");
        setClearOnNextInput(true);
      }
    }
  };

  const buttonPressed = (value: ButtonValue) => {
    switch (value.type) {
      case "reset": {
        setExpression("");
        setInputValue("");
        break;
      }
      case "number": {
        setInputValue(
          !clearOnNextInput ? inputValue + value.value : value.value
        );
        if (clearOnNextInput) setClearOnNextInput(false);
        break;
      }

      case "operation": {
        handleOperation(value.value);
        break;
      }
    }
    setLastButtonPressed(value.type);
  };
  return (
    <>
      <Flex justify="center">
        <Stack
          w="60%"
          mt={4}
          background={stackBackgroundColor}
          p={4}
          borderRadius="lg"
        >
          <Text textAlign="right" minH="24px">
            {expression}
          </Text>
          <Input disabled value={inputValue} textAlign="right" />
          <Box p={0} m={0}>
            <Button
              w="25%"
              type="reset"
              borderRadius="none"
              background={topRowButtonsBackgroundColor}
              onClick={() => buttonPressed({ value: "C", type: "reset" })}
            >
              C
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              background={topRowButtonsBackgroundColor}
              disabled={disableOperation()}
              onClick={() => {
                buttonPressed({ value: "±", type: "operation" });
                setLastButtonPressed("number");
              }}
            >
              ±
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              background={topRowButtonsBackgroundColor}
              disabled={disableOperation()}
              onClick={() => buttonPressed({ value: "%", type: "operation" })}
            >
              %
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              background="red.500"
              disabled={disableOperation()}
              onClick={() => {
                buttonPressed({ value: "/", type: "operation" });
              }}
            >
              /
            </Button>
          </Box>

          <Box p={0} m={0} style={{ marginTop: "0" }}>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "7", type: "number" });
              }}
            >
              7
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "8", type: "number" });
              }}
            >
              8
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "9", type: "number" });
              }}
            >
              9
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              background="red.500"
              disabled={disableOperation()}
              onClick={() => {
                buttonPressed({ value: "*", type: "operation" });
              }}
            >
              X
            </Button>
          </Box>

          <Box p={0} m={0} style={{ marginTop: "0" }}>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "4", type: "number" });
              }}
            >
              4
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "5", type: "number" });
              }}
            >
              5
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "6", type: "number" });
              }}
            >
              6
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              background="red.500"
              disabled={disableOperation()}
              onClick={() => {
                buttonPressed({ value: "-", type: "operation" });
              }}
            >
              -
            </Button>
          </Box>

          <Box p={0} m={0} style={{ marginTop: "0" }}>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "1", type: "number" });
              }}
            >
              1
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "2", type: "number" });
              }}
            >
              2
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "3", type: "number" });
              }}
            >
              3
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              background="red.500"
              disabled={disableOperation()}
              onClick={() => {
                buttonPressed({ value: "+", type: "operation" });
              }}
            >
              +
            </Button>
          </Box>

          <Box p={0} m={0} style={{ marginTop: "0" }}>
            <Button
              w="50%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: "0", type: "number" });
              }}
            >
              0
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              onClick={() => {
                buttonPressed({ value: ".", type: "number" });
              }}
            >
              .
            </Button>
            <Button
              w="25%"
              borderRadius="none"
              background="orange.500"
              disabled={disableOperation()}
              onClick={() => {
                buttonPressed({ value: "=", type: "operation" });
              }}
            >
              =
            </Button>
          </Box>
        </Stack>
      </Flex>
    </>
  );
}

export default App;
